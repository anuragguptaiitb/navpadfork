function [ entropy ] = calculate_entropy_bernoulli( x )
%CALCULATE_ENTROPY_BERNOULLI Summary of this function goes here
%   Detailed explanation goes here

entropy = real(-(x.*log(x) + (1-x).*log(1-x)));

entropy(isnan(entropy)) = 0; 
end

