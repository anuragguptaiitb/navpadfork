%% Example: Greedily select paths without safety considerations
clc
clear 
close all

%% Add required paths
addpath(genpath('../../maps'));
addpath(genpath('../../gridmapping'));
addpath(genpath('../../rayCasting'));
addpath(genpath('../../infoGain'));

%% Load a map
map = getMapfromImage('../../maps/example_map.tif');

%% Grid parameters
grid_params = struct();
grid_params.p_occ_default = 0.5;                                %default occupancy value
grid_params.p_empty_default = 1 - grid_params.p_occ_default;  
grid_params.p_occ_sensor = 0.95;                                %occupancy increase on hit
grid_params.p_empty_sensor = 0.60;                              %occupancy decrease on miss


%% Laser Parameters
laser_params = struct();
laser_params.max_range = 20;                                %laser range
laser_params.range_resolution = 0.8;                        %range resolution
laser_params.laser_resolution = 0.1;                        %angle resolution
laser_params.laser_fov = [-0.05,0.05];                      %field of view

%% Initialize grid
% Pre-Calculating grid update numbers
grid_params.log_odds_occ = log(grid_params.p_occ_default/grid_params.p_empty_default);
grid_params.log_odds_occ_sensor = log(grid_params.p_occ_sensor/(1-grid_params.p_occ_sensor));
grid_params.log_odds_empty_sensor = log((1-grid_params.p_empty_sensor)/grid_params.p_empty_sensor);

% Initializing maps and grids
world_map.data = map;
world_map.scale = 1;
world_map.min = [1,1];
robo_map = grid_params.log_odds_occ*ones(size(map));  

%% Initialize problem statement
state.x = 200; 
state.y = 250;
state.psi = 0;
bounding_box = [-50,-50;50,50];
robo_map = set_boundingbox_to_value( -100, robo_map, [state.x state.y], bounding_box, world_map);

%% Calcualting InformationGain
[X,Y] = meshgrid(1:30:size(map,1),1:30:size(map,2));
pose = [X(:) Y(:) zeros(size(X(:)))];
info_gain =  estimate_info_gain(robo_map,grid_params,world_map,pose,laser_params);

%% Adding display
scatter(pose(:,1),pose(:,2),30,info_gain(:),'filled')
figure
info_gain = reshape( info_gain,size(X));
imagesc(info_gain)
