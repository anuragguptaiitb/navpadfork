function handle_set = plot_path_set( path_set, handle_set )
%PLOT_PATH_SET Summary of this function goes here
%   Detailed explanation goes here

if (nargin < 2)
    handle_set = [];
    hold on;
    for path = path_set
        handle_set = [handle_set plot(path.x, path.y,'b')];
    end
else
    for i = 1:length(path_set)
        set(handle_set(i), 'XData', path_set(i).x, 'YData', path_set(i).y); 
    end
end

end

